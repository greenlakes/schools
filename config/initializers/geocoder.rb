Geocoder.configure(
  # Geocoding options
  # timeout: 3,                 # geocoding service timeout (secs)
  # lookup: :google,            # name of geocoding service (symbol)
  
  lookup: :bing,            # name of geocoding service (symbol)
  ip_lookup: :freegeoip,      # name of IP address geocoding service (symbol)
  # language: :en,              # ISO-639 language code
  use_https: false,           # use HTTPS for lookup requests? (if supported)
  # http_proxy: nil,            # HTTP proxy server (user:pass@host:port)
  # https_proxy: nil,           # HTTPS proxy server (user:pass@host:port)
  # api_key: "AIzaSyDqaPL5sIcIKGbVKXt7yj5YgBKxoCqJlj8",               # API key for geocoding service
  
  api_key: "Arwy7cpXwKz-qrtWzV5hXCuTAgRpNjmR-IvDojvFPq50k8xsdK_5r9rywhI3vWrz",               # API key for geocoding service
  # cache: Redis.new,                 # cache object (must respond to #[], #[]=, and #del)
  # cache_prefix: 'geocoder:',  # prefix (string) to use for all cache keys

  # Exceptions that should not be rescued by default
  # (if you want to implement custom error handling);
  # supports SocketError and Timeout::Error
  # always_raise: [],
  timeout: 20
  # Calculation options
  # units: :mi,                 # :km for kilometers or :mi for miles
  # distances: :linear          # :spherical or :linear
)