require 'csv'
namespace :schools do
  desc "pull schools general info (england_spine) into database"
  task seed_spine: :environment do


    # School.destroy_all

    # p "tables emptied"
    CSV.foreach("lib/assets/england_spine.csv", :headers =>true) do |row |
      puts row.inspect #just so that we know the file's being read

      #create new model instances with the data
      School.create!(
      urn: row[0].to_i,
      urn_ac: row[28].to_i,
      la: row[1].to_i,
      name: row[4],
      other_name: row[5],
      street: row[6],
      locality: row[7],
      address3: row[8],
      town: row[9],
      postcode: row[10],
      telnum: row[11],
      minorgroup: row[16],
      nftype: row[17],
      isprimary: row[19].to_bool,
      issecondary: row[20].to_bool,
      ispost16: row[21].to_bool,
      agel: row[22].to_i,
      ageh: row[23].to_i,
      gender: row[24],
      reldenom: row[26],
      admpol: row[27]
    )
    
 
    end
  end
end 