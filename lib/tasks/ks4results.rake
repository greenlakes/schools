require 'csv'
namespace :ks4results do
  desc "pull schools main ks4 results into database"
  task seed_ks4results: :environment do

    Ks4result.destroy_all
   
    p "tables emptied"
    
    CSV.foreach("lib/assets/england_ks4revised.csv", :headers =>true) do |row|


      puts row.inspect #just so that we know the file's being read
         
         
      school_temp = row[4]
      school = School.where(["urn = ?", school_temp])
      school = school[0]
        
        
        Ks4result.create!(
          
        school_id: school.id,
        urn: school_temp.to_i,
        
        attain8_score: row[57].to_f,
        prog8_score: row[74].to_f,
        grade5_or_above: row[90].to_f
        
         
        )
    
    end
  end
end 