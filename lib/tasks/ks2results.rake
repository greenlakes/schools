require 'csv'
namespace :ks2results do
  desc "pull schools main ks2 results into database"
  task seed_ks2results: :environment do

    Ks2result.destroy_all
   
    p "tables emptied"
    
    CSV.foreach("lib/assets/england_ks2revised.csv", :headers =>true) do |row|


      puts row.inspect #just so that we know the file's being read
         
         
      school_temp = row[4]
      school = School.where(["urn = ?", school_temp])
      school = school[0]
        
        
        Ks2result.create!(
          
        school_id: school.id,
        urn: school_temp.to_i,
        
        exp_standard: row[48].to_i,
        higher_standard: row[49].to_i,
        
        read_progress: row[50].to_f,
        write_progress: row[54].to_f,
        mat_progress: row[58].to_f,
    
        read_average: row[65].to_i,
        mat_average: row[73].to_i,
        read_average_prev: row[288].to_i,
        mat_average_prev: row[289].to_i
         
        )
        
        
    end
  end
end 