class Users::RegistrationsController < Devise::RegistrationsController
  # extended devise gem so that users sighning up with premium account (plan id = 2) - they will be saved with subscription method
  
  before_action :select_plan, only: :new
  
  def create
    
    super do |resource|
      if params[:plan]
        resource.plan_id = params[:plan]
        if resource.plan_id == 8
          resource.save_with_subscription
          UserMailer.new_user_notification(@user).deliver
        else
          resource.save
          UserMailer.new_user_notification(@user).deliver
        end
      end
    end
  end
  
  private
  def select_plan
    unless (params[:plan] == '7' || params[:plan] == '8')
      flash[:notice] = "Please select a membership plan to sign up."
      redirect_to root_url
    end
  end
  
end