class SchoolsController < ApplicationController
  before_action :set_school, only: [:edit, :update, :destroy]
  before_action :authenticate_admin, except: [:index, :show]

  # GET /schools
  # GET /schools.json
  def index
    @schools = if params[:l]
                      sw_lat, sw_lng, ne_lat, ne_lng = params[:l].split(",")
                      center   = Geocoder::Calculations.geographic_center([[sw_lat, sw_lng], [ne_lat, ne_lng]])
                      distance = Geocoder::Calculations.distance_between(center, [sw_lat, sw_lng])
                      box      = Geocoder::Calculations.bounding_box(center, distance)
                      School.within_bounding_box(box)
                    elsif params[:near]
                      if School.near(params[:near], 1.5).any?
                        School.near(params[:near], 1.5)
                      else
                        School.near('London', 2)
                      end
                    else
                      @location_info = request.location
                      if @location_info && School.near([@location_info.latitude, @location_info.longitude], 2).any?
                        School.near([@location_info.latitude, @location_info.longitude], 2)
                      else
                        School.near('London', 2)
                      end
                    end
                    
    @schoolz = School.search(params[:keyword])
  end

  # GET /schools/1
  # GET /schools/1.json
  def show
    @schools = []
    @schools << School.friendly.find(params[:id])
    
    @exp_standard_local = "67%"
    @exp_standard_england = "61%"
    @higher_standard_local = "13%"
    @higher_standard_england = "9%"
    
    @maths_score_local = 105
    @maths_score_england = 104
    @reading_score_local = 106
    @reading_score_england = 104
    
    @attain8_score_local = 50
    @attain8_score_england = 44.6
    
    @prog8_score_local = 0
    @prog8_score_england = 0
    
    @grade5_or_above_local = "51.6%"
    @grade5_or_above_england = "39.6%"
    
    unless @schools[0].ks2result.nil? 
    @data1 = [
               ["This school", @schools[0].ks2result.exp_standard], 
               ["Local authority average", @exp_standard_local],
               ["England average", @exp_standard_england]
              ] 
    end
    
    unless @schools[0].ks2result.nil? 
    @data2 = [
               ["This school", @schools[0].ks2result.higher_standard], 
               ["Local authority average", @higher_standard_local],
               ["England average", @higher_standard_england]
              ] 
    end
    
    unless @schools[0].ks2result.nil? 
    @data3 = [
               ["This school", @schools[0].ks2result.mat_average], 
               ["Local authority average", @maths_score_local],
               ["England average", @maths_score_england]
              ] 
    end
    
    unless @schools[0].ks2result.nil? 
    @data4 = [
               ["This school", @schools[0].ks2result.read_average], 
               ["Local authority average", @reading_score_local],
               ["England average", @reading_score_england]
              ] 
    end

    unless @schools[0].ks2result.nil? 
    @data5 = [
               ["This school", @schools[0].ks2result.read_progress] 
               
              ] 
    end
    
    unless @schools[0].ks2result.nil? 
    @data6 = [
               ["This school", @schools[0].ks2result.write_progress] 
               
              ] 
    end
    
    unless @schools[0].ks2result.nil? 
    @data7 = [
               ["This school", @schools[0].ks2result.mat_progress] 
               
              ] 
    end
    
    
    @attain8_score_local = 50
    @attain8_score_england = 44.6
    
    @prog8_score_local = 0
    @prog8_score_england = 0
    
    @grade5_or_above_local = "51.6%"
    @grade5_or_above_england = "39.6%"
    
    unless @schools[0].ks4result.nil? 
    @ks4_data1 = [
               ["This school", @schools[0].ks4result.attain8_score], 
               ["Local authority average", @attain8_score_local],
               ["England average", @attain8_score_england]
              ] 
    end
    
    unless @schools[0].ks4result.nil? 
    @ks4_data2 = [
               ["This school", @schools[0].ks4result.prog8_score], 
               ["Local authority average", @prog8_score_local],
               ["England average", @prog8_score_england]
              ] 
    end

    unless @schools[0].ks4result.nil? 
    @ks4_data3 = [
               ["This school", @schools[0].ks4result.grade5_or_above], 
               ["Local authority average", @grade5_or_above_local],
               ["England average", @grade5_or_above_england]
              ] 
    end
    
    respond_to do |format|
    format.html
    format.json { render json: @schools[0].to_json(include: [:ks2result, :ks4result] )}
    end
    
  end

  # GET /schools/new
  def new
    @school = School.new
  end

  # GET /schools/1/edit
  def edit
  end

  # POST /schools
  # POST /schools.json
  def create
    @school = School.new(school_params)

    respond_to do |format|
      if @school.save
        format.html { redirect_to @school, notice: 'School was successfully created.' }
        format.json { render :show, status: :created, location: @school }
      else
        format.html { render :new }
        format.json { render json: @school.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /schools/1
  # PATCH/PUT /schools/1.json
  def update
    respond_to do |format|
      if @school.update(school_params)
        format.html { redirect_to @school, notice: 'School was successfully updated.' }
        format.json { render :show, status: :ok, location: @school }
      else
        format.html { render :edit }
        format.json { render json: @school.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /schools/1
  # DELETE /schools/1.json
  def destroy
    @school.destroy
    respond_to do |format|
      format.html { redirect_to schools_url, notice: 'School was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_school
      @school = School.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def school_params
      params.require(:school).permit(:urn, :integer, :la, :integer, :name, :other_name, :street, :locality, :address3, :town, :postcode, :telnum, :minorgroup, :nftype, :isprimary, :issecondary, :is, :post16, :agel, :ageh, :gender, :reldenom, :admpol, :read_average, :mat_average, :keyword)
    end
    
    def authenticate_admin
      unless current_user.try(:admin?)
        redirect_to '/', alert: 'You are not authorised to access that page'
      end
    end

end