class PagesController < ApplicationController
  
  before_action :authenticate_user!, only: :community
  
  def index
    @basic_plan = Plan.find(7)
    @premium_plan = Plan.find(8)
  end
  
  def community
  end
  
end
