class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  
  # whitelisted devise form fields so rails can process them - prevents hackers from sending other data and malicious code
  
  before_action :configure_permitted_parameters, if: :devise_controller?
  
  
  protected
    def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:sign_up) { |user| user.permit(:stripe_card_token, :email, :password, 
      :password_confirmation) }
    end
  
end
