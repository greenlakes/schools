class ApplicationMailer < ActionMailer::Base
  default from: 'no_reply@checkaschool.co.uk'
  layout 'mailer'
end
