/* global GMaps */ 
/* global MarkerClusterer */ 
/* global google */ 
/* global Turbolinks */ 

document.addEventListener("turbolinks:load", function() {

  var map = new GMaps({
    div: '#map',
    zoom: 12,
    minZoom: 12,
    maxZoom: 15,
    lat: 51.5074,
    lng: 0.1278,
  
    markerClusterer: function(map) {
        var markerCluster = new MarkerClusterer(map, [], {
            title: 'Location Cluster',
            maxZoom: 12,
            imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
        });
        return markerCluster;
    }
    
  });
  
  
  
  
  window.map = map;

  var schools = JSON.parse(document.querySelector("#map").dataset.schools);
  window.schools = schools;

   var bounds = new google.maps.LatLngBounds();

  schools.forEach(function(school) {
    if (school.latitude && school.longitude) {
      var marker = map.addMarker({
        lat: school.latitude,
        lng: school.longitude,
        title: school.name,
        infoWindow: {
          content: `<p><a href='/schools/${school.urn}'>${school.name}</a></p>`
        }
      });

       bounds.extend(marker.position);
    }
  });


   var l = document.querySelector("#map").dataset.l;
  if (l) {
    var latlngs   = l.split(',');
    var southWest = new google.maps.LatLng(latlngs[0], latlngs[1]);
    var northEast = new google.maps.LatLng(latlngs[2], latlngs[3]);
    var bounds    = new google.maps.LatLngBounds(southWest, northEast);
    map.setOptions({ maxZoom: 15 });
    map.fitBounds(bounds);
  } else {
    map.setOptions({ maxZoom: 16 });
    map.fitZoom();
  }

  document.querySelector("#redo-search").addEventListener("click", function(e) {
    e.preventDefault();
    var bounds = map.getBounds();
    var location = bounds.getSouthWest().toUrlValue() + "," + bounds.getNorthEast().toUrlValue();

    Turbolinks.visit(`/schools?l=${location}`);
    
  
  });
  
  
 });
 
 