class School < ApplicationRecord
has_one :ks2result
has_one :ks4result

extend FriendlyId
friendly_id :urn, use: :slugged
alias_attribute :slug, :urn

  geocoded_by :address
  after_validation :geocode, if: :address_changed?
  
  
  def address
    [street, locality, address3, town, postcode].compact.join(', ')
  end

  def address_changed?
    street_changed? || town_changed? || locality_changed? || address3_changed? || postcode_changed?
  end
  
  def self.search(keyword)
  
    if keyword
      where("LOWER(name) LIKE ?", "%#{keyword.downcase}%").order('NAME').limit(50)
    end
  
  end

end