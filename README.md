
# README

Step1

1. School scaffold
2. Edit migration to add all the needed columns to schools table
3. Add Gmaps.js library to asset pipeline
4. Add gem 'geocoder' and its code to school model
5. rails generate migration AddLatitudeAndLongitudeToSchool latitude:float longitude:float
6. rails generate geocoder:config and changing settings in config/initializers/geocoder.rb as needed

Step2

1. create schools.rake file
2. upload csv with test data
3. add gem "to_bool"
4. rails g migration AddUrnAcToSchool
5. rake schools:seed_surrey_spine to seed test data (with geocoder code commented out in school model to avoid Google API limit error)
6. rake geocode:all CLASS=School SLEEP=0.25 BATCH=100 to do bulk geocoding to comply with Google API limits

Step3

1. created maps.js file to separate our js google maps related js from views, putting markers on the map
2. added gem 'jquery-rails' as required by bootstrap
3. added gem 'bootstrap', '~> 4.0'
4. changed application.css to application.scss

Step4

1. created search form and all search logic in schools controller and map.js file 

Step5

1. added clustering
2. added gem 'devise', '~> 4.2'
3. added shared views folder and navbar partial, moved search from into navbar
4. customised devise views (new, registration, edit, passwords)

Step6

1. created Ks2results model with main attributes
2. created ks2results rake file
3. added gem 'chartkick', '~> 1.4', '>= 1.4.1'
4. added association between school and ks2result model

Step7

1. Created DB table for membership plans
2. User and plan association
3. Added plan variables, added plan params to home page options so to make sign up forms conditional on the params[:plan]
(we are going to use 2 different sign up form depending on the type of membership chosen by the user)
4. created basic mmbership signup form partial and premium form partial, the latter contains credit card fields to enable 
sale of premium membership
5. moved footer to a shared partial

Step8

1. added gem 'stripe', '~> 1.43'
2. created stripe account on stripe site and added 2 membership plans there
3. created stripe.rb file unde config/initializers to write in required stripe settings (API keys)
4. added gem 'figaro', '~> 1.1', '>= 1.1.1' for  ENV variables (to store stripe API keys securely)
5. added API keys to application.yml file

Step9

1. added stripe js library to application layout file 
2. added stripe public key as ENV variable to application layout file
3. added premium_form id to premium form navbar (to identify this in our js code)
4. added id to submit button in premium_form navbar so we can target it 
5. created users.js with code to prevent premium form submission to rails and sending card data to stripe server instead 
to obtain a card token
6.added stripe_customer_token to users table
7. added hidden fields to signup forms (to passmembership plan id)
8. in application_controller whitelisted form fields sent to rails so they are accepted
9. added  controllers: { registrations: 'users/registrations' } to routes file as we are using customised registration
10. created users folder under app/controllers as we need to extend devise
11. created custome registrations_controller.rb under app/controllers/ so that premium account users have special 
sign up process  - save_with_subscription
12. in user model added special registration process for premium users that includes call to stripe to setup a subscription
13. added hirb gem to visualise sqlite3 in rails console in development 
14. created a test user using stripe test card number 4000056655665556 cvs 123 and any date in the future as card valid to date
15. added respective comments to the files mentioned above

Step10

1. imporved flash messages appearance by adding custom styles in application.scss
2. added js to application.js to make flash dynamic
3. security improvement to prevent user from interfering with params in URL and signing without plan id - private select_plan
method added to custome registrations_controller under app/controllers/users
4. created production group in gem file and added gem 'pg', '~> 1.0' to production
5. moved gem 'sqlite3' to development group

Step11

1. Test deploy on heroku
2. Fixed errors with heroku deployment - commented out the line: config.assets.js_compressor = :uglifier 
in config/environments/production.rb since 'uglifier' (minifier) does not seem to be working correctly with the ES6 syntax.
3. Also running gem install bundler as advised by heroku to update bundler version in use

Step12

1. heroku addons:create mailgun:starter
2. added mail server settings to config/environment.rb

Step13

1. created mailer to send notifications when a new user registers - rails g mailer user_mailer new_user_notification
2. made necessary changed to mailer view file new_user_notification.txt under views/user_mail
3. added mailer method to registrations_controller - UserMailer.new_user_notification(@user).deliver
4. added  config.action_mailer.default_url_options = { :host => 'tranquil-oasis-26696.herokuapp.com' } to config/production.rb 
to fix an issue with heroku not sending password reset email 

Step14

1. added community page accessible for registered members only 
(added before_action :authenticate_user!, only: :community to pages_controller)
2. slight design changes (navbar and buttons)
3. added admin boolean column to users table
4. added admin user in console - "admin@example.com" 
5. added div with admin options (such as edit/delete school objects) to the top of school show page 
(if user_signed_in? && current_user.admin)
6. resolved merge conflicts 

Step15

1. added gem 'friendly_id', '~> 5.1.0' to gem file (we are goind to use school official identifier called URN) instead of 
default rails id in school objects URLs. This will enable us to connect pages for the newly-created academeis on the basis of
old schools so the users can refer to easily.
2. Generate the friendly configuration file - rails generate friendly_id
3. added required changes to migration file generated above 
4. rails db:migrate
5. added required FriendlyId code line to school model file.
6. changed show method in schools_controller to @schools << School.friendly.find(params[:id]) to implement FriendlyIDs 

Step16

1. created table (and all the needed columns/attributes) for Ks4results to be taken from open data file 
2. uploaded ks4results as CSV file (obtained from Department of Eduction website) to lib/assets
3. created Ks4result model file, added belongs_to :school association and added has_one :ks4result to school.rb accordingly
4. rails db:migrate
5. created ks4results.rake file
6. visualised ks2results using chartkick gem and chart.js library

Step17

1. corrected show view file to make data visualisation correct.
2. corrected maps zoom levels
3. implemented geolocation so schools index page starts from current user location for extra convenience
4. added font-awesome cdn for logo icon
5. corrected bootsrap design
6. improved code layout 
7. ran pages through validator.w3.org service to find extra errors and resolve them
8. solving an issue when target: '_blank' used with button_to did result in opening a new window as it turned out 
button_to helper is automatically wrapped in a form by rails. So we needed to pass the attribute target: '_blank' as 
form: {target: '_blank'} 
9. addind extra search form to enable search by school name in addition to location search inplemented earlier
10. adding search code to school model including limits in case is search is too generic such as search for school keyword only.