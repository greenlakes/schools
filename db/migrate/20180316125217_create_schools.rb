class CreateSchools < ActiveRecord::Migration[5.1]
  def change
    create_table :schools do |t|
      t.integer :urn
      t.integer :la
      t.string :name
      t.string :other_name
      t.string :street
      t.string :locality
      t.string :address3
      t.string :town
      t.string :postcode
      t.string :telnum
      t.string :minorgroup
      t.string :nftype
      t.boolean :isprimary
      t.boolean :issecondary
      t.boolean :ispost16
      t.integer :agel
      t.integer :ageh
      t.string :gender
      t.string :reldenom
      t.string :admpol

      t.timestamps
    end
  end
end
