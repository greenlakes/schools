class AddReadProgressAndWRiteProgressAndMatProgressToKs2results < ActiveRecord::Migration[5.1]
  def change
    add_column :ks2results, :read_progress, :float
    add_column :ks2results, :write_progress, :float
    add_column :ks2results, :mat_progress, :float
  end
end
