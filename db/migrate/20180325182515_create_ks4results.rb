class CreateKs4results < ActiveRecord::Migration[5.1]
  def change
    create_table :ks4results do |t|
      t.integer :urn
      t.float :attain8_score
      t.float :prog8_score
      t.float :grade5_or_above
      t.references :school

      t.timestamps
    end
  end
end
