class AddExpStandardAndHigherStandardToKs2result < ActiveRecord::Migration[5.1]
  def change
    add_column :ks2results, :exp_standard, :integer
    add_column :ks2results, :higher_standard, :integer
  end
end