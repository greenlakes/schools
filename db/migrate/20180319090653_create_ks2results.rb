class CreateKs2results < ActiveRecord::Migration[5.1]
  def change
    create_table :ks2results do |t|
      t.integer :urn
      t.integer :read_average
      t.integer :mat_average
      t.integer :read_average_prev
      t.integer :mat_average_prev
      t.references :school
      t.timestamps
    end
  end
end
